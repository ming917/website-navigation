import Vue from 'vue'
import App from './App'

import uView from 'uview-ui';
Vue.use(uView);

//全局数据仓库
import store from '@/store'
Vue.prototype.$store = store

import utils from 'utils/utils.js'
Vue.prototype.$utils = utils

import clickOutside from "@/directive/clickOutside.js";
Vue.use(clickOutside);

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	...App
})
app.$mount()
