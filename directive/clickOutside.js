// uniapp支持：H5,APP端V3。不支持：APP端旧版，微信小程序。


export default {
	install(Vue) {
		Vue.directive('click-outside', {
			// 初始化指令
			bind(el, binding, vnode) {
				function clickHandler(e) {
					// 这里判断点击的元素是否是本身，是本身，则返回
					if (el.contains(e.target)) {
						return false;
					}
					// 判断指令中是否绑定了函数
					if (binding.value) {
						// 如果绑定了函数 则调用那个函数，此处binding.value就是handleClose方法
						binding.value(e);
					}
				}
				// 给当前元素绑定个私有变量，方便在unbind中可以解除事件监听
				el.vueClickOutside = clickHandler;
				// 监听全局点击
				document.addEventListener('click', clickHandler);
			},
			unbind(el, binding) {
				// 解除事件监听
				document.removeEventListener('click', el.vueClickOutside);
				delete el.vueClickOutside;
			},
		})
	}
}
