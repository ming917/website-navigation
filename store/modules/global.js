import staticData from '@/static/data.js';


const globalModules = {
	state: {
		navList: null,
		contentList: null,
		searchList: null
	},
	mutations: {
		setNavList(state, data) {
			state.navList = data;
		},
		setContentList(state, data) {
			state.contentList = data;
		},
		setSearchList(state, data) {
			state.searchList = data;
		},
	},
	actions: {
		globalInit(context) {
			context.dispatch('getNavList');
			context.dispatch('getContentList');
			context.dispatch('getSearchList');
		},
		getNavList(context) {
			context.commit('setNavList', staticData)
		},
		getContentList(context) {
			let contentList = getContentData(staticData)
			context.commit('setContentList', contentList)
		},
		getSearchList(context) {
			let searchList = getSeachData(staticData)
			context.commit('setSearchList', searchList)
		}
	}
}
export default globalModules

// 搜索数据
function getSeachData(webData) {
	let arr = [];
	for (let web of webData) {
		if (web.children) {
			let arrAppend = getSeachData(web.children);
			arr.push(...arrAppend);
		}

		if (web.web) {
			arr.push(...web.web);
		}
	}
	return arr
}

// 主体数据
function getContentData(webData) {
	let arr = [];
	for (let web of webData) {
		if (web.children) {
			let arrAppend = getContentData(web.children);
			arr.push(...arrAppend);
		}

		if (web.web && web.web.length > 0) {
			arr.push({
				_id: web._id,
				name: web.name,
				web: web.web
			});
		}
	}
	return arr
}