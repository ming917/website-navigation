export default {
 	baseUrl: "https://unidemo.dcloud.net.cn/",
 	header: {
 		'Content-Type': 'application/json;charset=UTF-8',
 		'Content-Type': 'application/x-www-form-urlencoded'
 	},
 	dataType: "json",
 	/* 如设为json，会对返回的数据做一次 JSON.parse */
 	responseType: "text"
 }
