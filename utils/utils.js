export default {
	// 打开外部链接
	openLink(link) {
		// #ifdef APP-PLUS
		plus.runtime.openURL(link);
		// #endif
		// #ifdef H5
		window.open(link);
		// #endif
		// #ifdef MP
		uni.setClipboardData({
			data: link
		});
		// #endif
	}
}
