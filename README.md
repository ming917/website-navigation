# 对味网址导航

 - 这个项目是为了做一个适合自己的网址导航。
 - 项目设计参考webstack，项目框架：uniapp + uview。
 - 此项目仅在电脑和手机浏览器运行过。


![手机](https://gitee.com/ming917/image-store/raw/master/website-navigation/mobile_demo_1616751331701.png)


![电脑](https://gitee.com/ming917/image-store/raw/master/website-navigation/pc_demo_1616751310341.png)



## 预览

[**在线演示**](https://ming917.gitee.io/website-navigation/)


## 使用

&emsp;&emsp;最简单快速上线自己的导航网站，您可以通过修改本项目 static/data.js 进行自定义网址导航，同时网址图标的替换在 static/images/logos/ 中进行替换，修改后既可部署上线。



## 感谢

[**uView**](https://www.uviewui.com)

[**uni-app**](https://uniapp.dcloud.io)

[**WebStackPage**](http://webstack.cc/cn/index.html)



## 版权信息

MIT
